# -*- encoding: utf-8 -*-
"""
License: MIT
Copyright (c) 2019 - present AppSeed.us
"""

# Python modules
import os, logging, requests, json

# Flask modules
from flask               import Blueprint
# from flask               import render_template, request, url_for, session
# from flask               import Blueprint, redirect, send_from_directory
# from flask_login         import login_user, logout_user, current_user, login_required
# from werkzeug.exceptions import HTTPException, NotFound, abort

# # App modules
# from app        import app, lm, db, bc
# from app.models import User
# from app.forms  import LoginForm, RegisterForm


api_blueprint = Blueprint('api_blueprint', __name__)

@api_blueprint.route('/')
def index():
    return "This is an example app"
