# -*- encoding: utf-8 -*-
"""
License: MIT
Copyright (c) 2019 - present AppSeed.us
"""

# Python modules
import os, logging, requests, json

# Flask modules
from flask               import render_template, request, url_for, redirect, send_from_directory, session
from flask_login         import login_user, logout_user, current_user, login_required
from werkzeug.exceptions import HTTPException, NotFound, abort

# App modules
from app        import app, lm, db, bc
from app.models import User
from app.forms  import LoginForm, RegisterForm

# provide login manager with load_user callback
@lm.user_loader
def load_user(user_id):
    # print(User.query.get(int(user_id)))
    return User.query.get(int(user_id))


# API Handler
@app.route('/<cust>/api/', defaults={'u_path': ''})
@app.route('/<cust>/api/<path:u_path>')
def api(cust, u_path):
    if not current_user.is_authenticated:
        return redirect(url_for('login'))

    try:
        url = f"{app.config.get('API_URL')}/{cust}/{u_path}"
        print(' -', url)
        if request.method == 'GET':
            resp = requests.request(request.method,
                # "{}{}".format(url,)
                f"{url}{('?'+request.query_string.decode('utf-8')) if request.query_string else ''}",
                headers = {
                  'Auth': session.get('user', {}).get('token', '')
                })
        else:
            resp = requests.request(request.method,
                url,
                headers = {
                  'Auth': session.get('user', {}).get('token', '')
                },
                data={})

        
        # if r.status_code == 200:
        return (resp.text, resp.status_code, resp.headers.items())
    
    except Exception as e:
        return json.dumps({'status': repr(e)})


# Logout user
@app.route('/logout.html')
def logout():
    logout_user()
    User.query.filter_by(name=session.pop('user')['first_name']).delete()
    db.session.commit()
    return redirect(url_for('index'))

# # Register a new user
@app.route('/register.html', methods=['GET', 'POST'])
def register():
    
#     # cut the page for authenticated users
     if current_user.is_authenticated:
         return redirect(url_for('index'))

#     # declare the Registration Form
#     form = RegisterForm(request.form)

#     msg = None

#     if request.method == 'GET': 

#         return render_template( 'pages/register.html', form=form, msg=msg )

#     # check if both http method is POST and form is valid on submit
#     if form.validate_on_submit():

#         # assign form data to variables
#         username = request.form.get('username', '', type=str)
#         password = request.form.get('password', '', type=str) 
#         email    = request.form.get('email'   , '', type=str) 

#         # filter User out of database through username
#         user = User.query.filter_by(user=username).first()

#         # filter User out of database through username
#         user_by_email = User.query.filter_by(email=email).first()

#         if user or user_by_email:
#             msg = 'Error: User exists!'
        
#         else:         

#             pw_hash = password #bc.generate_password_hash(password)

#             user = User(username, email, pw_hash)

#             user.save()

#             msg = 'User created, please <a href="' + url_for('login') + '">login</a>'     

#     else:
#         msg = 'Input error'     

#     return render_template( 'pages/register.html', form=form, msg=msg )

# Authenticate user
@app.route('/login.html', methods=['GET', 'POST'])
def login():
    # cut the page for authenticated users
    if current_user.is_authenticated:
        return redirect(url_for('index'))
            
    # Declare the login form
    form = LoginForm(request.form)

    # Flask message injected into the page, in case of any errors
    msg = None

    # check if both http method is POST and form is valid on submit
    if form.validate_on_submit():

        # assign form data to variables
        username = request.form.get('username', '', type=str)
        password = request.form.get('password', '', type=str) 

        # filter User out of database through username
        # user = User.query.filter_by(user=username).first()

        r = requests.request("POST",
            app.config.get('API_URL') + '/db/login',
            headers={'Content-Type': 'application/json'},
            data=json.dumps({'loginname': username, 'password': password}))

        s = json.loads(r.text)
        
        if r.status_code == 200:
            user = User.query.filter_by(user=username).first()
            if user:
                db.session.delete(user)
                db.session.commit()
            user = User(username, s['data']['first_name'], s['data']['token'])
            print(s['data']['token'], "----------")
            user.save()
            login_user(user)
            session['user'] = s['data']
            return redirect(url_for('index'))
        else:
            msg = s['meta']['message']

        # if user:
            
        #     #if bc.check_password_hash(user.password, password):
        #     if user.password == password:
        #         login_user(user)
        #         return redirect(url_for('index'))
        #     else:
        #         msg = "Wrong password. Please try again."
        # else:
        #     msg = "Unknown user"

    return render_template( 'pages/login.html', form=form, msg=msg )


@app.route('/', defaults={'path': 'index'})
@app.route('/<path>')
def index(path):

    if not current_user.is_authenticated:
        return redirect(url_for('login'))

    content = None

    try:

        # try to match the pages defined in -> pages/<input file>
        return render_template( 'pages/%s.html' % path )
    
    except:
        
        return render_template( 'pages/error-404.html' )

# Return sitemap 
@app.route('/sitemap.xml')
def sitemap():
    return send_from_directory(os.path.join(app.root_path, 'static'), 'sitemap.xml')
